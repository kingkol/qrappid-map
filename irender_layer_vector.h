﻿#ifndef IRENDER_LAYER_VECTOR_H
#define IRENDER_LAYER_VECTOR_H

#include "irender_layer_base.h"

class iRender_layer_vector : public iRender_layer_base
{
public:
    iRender_layer_vector();

    void add_eclipse(Coordinate cent, float radius);
};

#endif  // IRENDER_LAYER_VECTOR_H
