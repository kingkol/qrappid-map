#ifndef ISOURCE_TILE_IMPL_H
#define ISOURCE_TILE_IMPL_H
extern "C"
{
#include <sqlite3.h>
};

#include <io.h>
#include <stdint.h>

#include <memory>
#include <string>
#include <vector>

class imagetile
{
public:
    typedef std::vector<imagetile> Contianer;

public:
    imagetile() : tile_column(0), tile_row(0), zoom_level(0), sp_blob(0) {}
    imagetile(int x, int y, int z) : tile_column(x), tile_row(y), zoom_level(z), sp_blob(0) {}
    ~imagetile() { release(); }
    void release()
    {
        if (sp_blob) sp_blob.reset();
        sp_blob = 0;
    }

    bool loadfromfile(const char* filepath)
    {
        FILE* pf = fopen(filepath, "rb+");
        if (!pf) return false;

        blob_size = _filelength(_fileno(pf));
        sp_blob   = std::shared_ptr<uint8_t>(new uint8_t[blob_size]);
        fread(sp_blob.get(), 1, blob_size, pf);
        fclose(pf);
        return true;
    }

public:
    int                      tile_column;
    int                      tile_row;
    char                     zoom_level;
    std::shared_ptr<uint8_t> sp_blob;
    int                      blob_size;
};

class isource_tile_impl
{
public:
    isource_tile_impl();

    void imagetile_to_mbtiles();

    void insert_imagetile_tombtiles(sqlite3* db, imagetile::Contianer* tiles);
};

#endif  // ISOURCE_TILE_IMPL_H
