﻿#include <QApplication>
#include <QDebug>

#include "isource_tile_impl.h"
#include "mainwindow.h"
#include "rappidprojection.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
#if 1
    MainWindow w;
    w.show();
#else
    isource_tile_impl _til;
    _til.imagetile_to_mbtiles();
#endif
    return a.exec();
}
