#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "irender_layer_base.h"

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow                     *ui;
    iRender_layer::iHRender_layer       bglayer;
    iRender_geometry::iHRender_geometry pyaw;
};
#endif  // MAINWINDOW_H
