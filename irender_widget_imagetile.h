﻿#ifndef IRENDER_WIDGET_IMAGETILE_H
#define IRENDER_WIDGET_IMAGETILE_H

#include <QOpenGLWidget>

#include "irender_geometry.h"
#include "irender_layer_base.h"
#include "isource_imagetile.h"
class iRender_widget_imagetile : public QOpenGLWidget
{
    Q_OBJECT
public:
    explicit iRender_widget_imagetile(QWidget *parent = nullptr);
signals:
    void signal_update();

public:
    void addlayer(iRender_layer::iHRender_layer ly) { _layers.push_back(ly); }
    void centerOn(double lon, double lat);

protected:
    void drawImageTile(QPainter *painter);
    void paintEvent(QPaintEvent *event) override;
    void wheelEvent(QWheelEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void mouseDoubleClickEvent(QMouseEvent *event) override;

    QPointF getMouseLnglat(QPoint &mouseloc);

private:
    int    zoomlevel;
    double mapresolution;
    bool   mousemoved;
    bool   mousepressed;

    QPoint             mouse_wheel_pos;
    QPointF            mouse_wheel_blh;
    QPoint             mouse_pressed_pos;
    QPointF            mapcenter_blh;
    QRect              viewport_widget;
    QPointF            left_top_pixel_draw;
    QPointF            left_top_pixel_draw_old;
    QRect              viewport_tilelist;
    isource_tile *_source;
    tileDB viewedTiles;

private:
    iRender_painter          _render_painter;
    iRender_layer::Container _layers;
};

#endif  // IRENDER_WIDGET_IMAGETILE_H
