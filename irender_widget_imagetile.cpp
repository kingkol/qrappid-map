﻿#include "irender_widget_imagetile.h"

#include <QDebug>
#include <QPaintEvent>
#include <QPainter>

#include "rappidprojection.h"
// left top      180°, 85.0511°
// right bottom -180°,-85.0511°

#define CANVAS_SCALE 1
#define CANVAS_SCALE_RATE 0.5

iRender_widget_imagetile::iRender_widget_imagetile(QWidget *parent)
    : QOpenGLWidget(parent), zoomlevel(18), mouse_pressed_pos(0, 0), mapcenter_blh(114., 60.), left_top_pixel_draw(0., 0.)
{
    mousemoved   = true;
    mousepressed = false;
    _source      = 0;
    connect(this, &iRender_widget_imagetile::signal_update, this, [=]() { update(); });
    setMouseTracking(true);
}

void iRender_widget_imagetile::wheelEvent(QWheelEvent *event)
{
    //获取鼠标悬停位置对应的经纬度
    //鼠标没移动的话只获取一次
    if (mousemoved) {
        mouse_wheel_pos = event->pos();
        mouse_wheel_blh = getMouseLnglat(mouse_wheel_pos);
    }
    mousemoved = false;

    double zoom_result = zoomlevel;

    zoom_result += (event->angleDelta().y() > 0) ? 1 : -1;
    if (zoom_result < 3 || zoom_result > 18) return;
    zoomlevel = zoom_result;

    qDebug() << mouse_wheel_blh;

    //计算层级变更后鼠标悬停位置像素坐标
    int lngpixel = RappidProjection::lngToPixel(mouse_wheel_blh.x(), zoomlevel);
    int latpixel = RappidProjection::latToPixel(mouse_wheel_blh.y(), zoomlevel);
    //更新左上角点像素点值
    left_top_pixel_draw.setX(-lngpixel + mouse_wheel_pos.x());
    left_top_pixel_draw.setY(-latpixel + mouse_wheel_pos.y());

    emit signal_update();
}

void iRender_widget_imagetile::mouseMoveEvent(QMouseEvent *event)
{
    //标记鼠标移动了，当鼠标滚轮缩放地图时重新获取鼠标悬停位置
    mousemoved = true;
    //由于开启了setMouseTracking，这里只有鼠标按下后才移动地图
    if (mousepressed) {
        QPointF deta        = event->pos() - mouse_pressed_pos;
        left_top_pixel_draw = left_top_pixel_draw_old + deta;

        QPointF centra = -left_top_pixel_draw + QPoint(viewport_widget.width() * CANVAS_SCALE_RATE, viewport_widget.height() * CANVAS_SCALE_RATE);
        mapcenter_blh.setX(RappidProjection::pixelToLng(centra.x(), zoomlevel));
        mapcenter_blh.setY(RappidProjection::pixelToLat(centra.y(), zoomlevel));

        emit signal_update();
    }
}

void iRender_widget_imagetile::mouseDoubleClickEvent(QMouseEvent *event)
{
    //地图双击选点
    QPoint mouseloc = event->pos();
    qDebug() << "clicked:" << getMouseLnglat(mouseloc);
}

void iRender_widget_imagetile::mousePressEvent(QMouseEvent *event)
{
    mousepressed            = true;
    mouse_pressed_pos       = event->pos();
    left_top_pixel_draw_old = left_top_pixel_draw;
}

void iRender_widget_imagetile::mouseReleaseEvent(QMouseEvent *event) { mousepressed = false; }

void iRender_widget_imagetile::centerOn(double lon, double lat)
{
    //设置地图中心点
    int lngpixel = RappidProjection::lngToPixel(lon, zoomlevel);
    int latpixel = RappidProjection::latToPixel(lat, zoomlevel);
    left_top_pixel_draw.setX(-lngpixel + viewport_widget.width() * CANVAS_SCALE_RATE);
    left_top_pixel_draw.setY(-latpixel + viewport_widget.height() * CANVAS_SCALE_RATE);
}

void iRender_widget_imagetile::paintEvent(QPaintEvent *event)
{
    viewport_widget     = event->rect();
    mapresolution       = RappidProjection::getresolution(mapcenter_blh.y(), zoomlevel);
    static bool firstin = true;
    if (firstin) {
        mapcenter_blh = QPointF(114, 30);
        centerOn(mapcenter_blh.x(), mapcenter_blh.y());
        firstin = false;
    }

    QPainter painter;

    _render_painter.ptr_painter     = &painter;
    _render_painter.ptr_paint_event = event;
    _render_painter.ptr_offset      = &left_top_pixel_draw;
    _render_painter.ptr_zoomlevel   = &zoomlevel;

    painter.begin(this);

    // painter.scale(CANVAS_SCALE, CANVAS_SCALE);

    painter.fillRect(event->rect(), QColor(252, 249, 242));

    drawImageTile(&painter);

    painter.setRenderHint(QPainter::Antialiasing, true);
    painter.setRenderHint(QPainter::SmoothPixmapTransform, true);
    for (size_t i = 0; i < _layers.size(); ++i) {
        _layers[i]->_painter_handle = &_render_painter;
        _layers[i]->paint();
    }

    painter.setPen(QPen(Qt::red, 2, Qt::PenStyle::SolidLine));
    //绘制地图缩放文字
    painter.drawText(10, viewport_widget.height() - 5, QString::asprintf("%d", zoomlevel));
    //绘制地图中心十字
    painter.drawLine(viewport_widget.width() * CANVAS_SCALE_RATE - 5, viewport_widget.height() * CANVAS_SCALE_RATE,
                     viewport_widget.width() * CANVAS_SCALE_RATE + 5, viewport_widget.height() * CANVAS_SCALE_RATE);
    painter.drawLine(viewport_widget.width() * CANVAS_SCALE_RATE, viewport_widget.height() * CANVAS_SCALE_RATE - 5, viewport_widget.width() * CANVAS_SCALE_RATE,
                     viewport_widget.height() * CANVAS_SCALE_RATE + 5);

    painter.setPen(QPen(Qt::blue, 2, Qt::PenStyle::SolidLine));

    painter.end();
}

void iRender_widget_imagetile::drawImageTile(QPainter *painter)
{
    int skip_tile_x    = floor(left_top_pixel_draw.x() / 256.) + 1;
    int skip_tile_y    = floor(left_top_pixel_draw.y() / 256.) + 1;
    int viewx_tile_cnt = ceilf(viewport_widget.width() / 256.) + 1;
    int viewy_tile_cnt = ceilf(viewport_widget.height() / 256.) + 1;
    if (skip_tile_x < 0) {
        viewport_tilelist.setX(-skip_tile_x);
        viewport_tilelist.setWidth(-skip_tile_x + viewx_tile_cnt);
    } else {
        viewport_tilelist.setX(skip_tile_x);
        viewport_tilelist.setWidth(-skip_tile_x + viewx_tile_cnt);
    }

    if (skip_tile_y < 0) {
        viewport_tilelist.setY(-skip_tile_y);
        viewport_tilelist.setHeight(-skip_tile_y + viewy_tile_cnt);
    } else {
        viewport_tilelist.setY(skip_tile_y);
        viewport_tilelist.setHeight(-skip_tile_y + viewy_tile_cnt);
    }

    if (!_source) {
        _source = new isource_tile_http_arcgis();
    }

    for (int x = viewport_tilelist.x(); x < viewport_tilelist.width(); ++x) {
        for (int y = viewport_tilelist.y(); y < viewport_tilelist.height(); ++y) {
            QString tileid = tilekey::keytostr(x, y, zoomlevel);
            if (!viewedTiles.contains(tileid)){
                viewedTiles.insert(tileid,tilekey(x,y,zoomlevel));
            }

            //            QPoint pstart(x * 256, y * 256);
            //            pstart += left_top_pixel_draw.toPoint();

            //            pim = _source->get_tile(tilekey(x, y, zoomlevel));
            //            if (pim) {
            //                // 绘制地图瓦片
            //                painter->drawImage(pstart, *pim);
            //                // 绘制tile边界
            //                // painter->drawRect(pstart.x(), pstart.y(), 256, 256);
            //            }
        }
    }
    _source->get_tiles(&viewedTiles);
    tilekey::tp =left_top_pixel_draw;
    tileDB::iterator _itr =viewedTiles.begin();
    while (_itr != viewedTiles.end()){
        _itr->draw(painter);
        _itr++;
    }
}

QPointF iRender_widget_imagetile::getMouseLnglat(QPoint &mouseloc)
{
    QPointF lnglat = -left_top_pixel_draw + mouseloc;
    lnglat.setX(RappidProjection::pixelToLng(lnglat.x(), zoomlevel));
    lnglat.setY(RappidProjection::pixelToLat(lnglat.y(), zoomlevel));
    return lnglat;
}
