﻿#include "rappidprojection.h"

#include <math.h>

#include <QDebug>

#define E 2.7182818459

RappidProjection::RappidProjection() {}
int RappidProjection::long2tilex(double lon, int z) { return (int)(floor((lon + 180.0) / 360.0 * (1 << z))); }

int RappidProjection::lat2tiley(double lat, int z)
{
    double latrad = lat * M_PI / 180.0;
    return (int)(floor((1.0 - asinh(tan(latrad)) / M_PI) / 2.0 * (1 << z)));
}

double RappidProjection::tilex2long(int x, int z) { return x / (double)(1 << z) * 360.0 - 180; }

double RappidProjection::tiley2lat(int y, int z)
{
    double n = M_PI - 2.0 * M_PI * y / (double)(1 << z);
    return 180.0 / M_PI * atan(0.5 * (exp(n) - exp(-n)));
}

void RappidProjection::lonLat2Mercator(double lng, double lat, double &mcx, double &mcy)
{
    double x = lng * EARTH_N_Q_180;
    double y = log(tan((90. + lat) * M_PI_Q_360)) / M_PI_Q_180;
    y        = y * EARTH_N_Q_180;
    mcx      = x;
    mcy      = y;
}

void RappidProjection::Mercator2lonLat(double mcx, double mcy, double &lng, double &lat)
{
    double x = mcx * Q_180_EARTH_N;
    double y = mcy * Q_180_EARTH_N;
    y        = Q_180_M_PI * (2 * atan(exp(y * M_PI_Q_180)) - M_PI_Q_2);
    lng      = x;
    lat      = y;
}

double RappidProjection::getresolution(double lat, int z)
{
    double meters_per_pixel = EQUATOR_LENGTH * 1000. / 256;
    // m/pixel
    return meters_per_pixel * cos(lat * M_PI_Q_180) / pow(2, z);
}
double RappidProjection::getscale(int screendpi, double reso) { return screendpi * 1. / 0.0254 * reso; }

double RappidProjection::lngToPixel(double lng, int zoom) { return (lng + 180.) * (256L << zoom) / 360.; }
double RappidProjection::pixelToLng(double pixelX, int zoom) { return pixelX * 360. / (256L << zoom) - 180.; }
double RappidProjection::latToPixel(double lat, int zoom)
{
    double siny = sin(lat * M_PI / 180);

    double y = log((1 + siny) / (1 - siny));

    return (128 << zoom) * (1 - y / (2 * M_PI));
}
double RappidProjection::pixelToLat(double pixelY, int zoom)
{
    double y = 2 * M_PI * (1 - pixelY / (128 << zoom));

    double z = pow(E, y);

    double siny = (z - 1) / (z + 1);

    return asin(siny) * 180 / M_PI;
}
