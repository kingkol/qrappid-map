﻿#include "isource_tile_impl.h"

#include <QDebug>
#include <QDir>
#include <QFileInfoList>

isource_tile_impl::isource_tile_impl() {}

void isource_tile_impl::imagetile_to_mbtiles()
{
    sqlite3* db       = 0;
    char     buf[512] = "";
    // *.mbtiles
    // metadata
    // tiles
    // std::string str_tablename  = "tiles";
    std::string str_dbname     = QStringLiteral("世界").toStdString();
    std::string str_tileroot   = R"(D:\TData\TileMapData\WXWP\)" + str_dbname;
    std::string str_dbfilepath = str_tileroot + ".mbtiles";

    char* errorMessage = NULL;
    int   status       = sqlite3_open(str_dbfilepath.c_str(), &db);
    printf("sqlite3_open %d\n", status);

    if (SQLITE_OK != status) {
        printf("open db file(%s) fatl\n", str_dbfilepath.c_str());
        return;
    }

    sprintf(buf, "drop table if exists metadata");
    sqlite3_exec(db, buf, 0, 0, 0);
    sprintf(buf, "create table metadata (name text, value text)");
    status = sqlite3_exec(db, buf, NULL, NULL, &errorMessage);
    if (SQLITE_OK != status) {
        printf("create metadata table fatl\n");
        sqlite3_close(db);
        return;
    }
    sprintf(buf, "insert into metadata values('format', 'png')");
    sqlite3_exec(db, buf, 0, 0, 0);

    sprintf(buf, "drop table if exists tiles");
    sqlite3_exec(db, buf, 0, 0, 0);

    sprintf(buf, "create table tiles (zoom_level integer , tile_column integer , tile_row integer, tile_data blob)");
    status = sqlite3_exec(db, buf, NULL, NULL, &errorMessage);
    if (SQLITE_OK != status) {
        printf("create tiles table fatl\n");
        sqlite3_close(db);
        return;
    }

    sprintf(buf, "create UNIQUE INDEX tiles_idx on tiles (zoom_level , tile_column , tile_row )");
    status = sqlite3_exec(db, buf, NULL, NULL, &errorMessage);
    if (SQLITE_OK != status) {
        printf("create table table fatl\n");
        sqlite3_close(db);
        return;
    }

    imagetile::Contianer _tiles;

    // png tile image is  zyx
    //  z y-row x-column
    QDir          dir_tileroot(str_tileroot.c_str());
    QFileInfoList dir_zoom_level = dir_tileroot.entryInfoList(QDir::Dirs | QDir::NoSymLinks | QDir::NoDotAndDotDot);
    for (int iz = 0; iz < dir_zoom_level.size(); ++iz) {
        // zoom_level
        int z = dir_zoom_level[iz].baseName().toInt();
        qDebug() << dir_zoom_level[iz].baseName();
        QDir          dir_tmp_1(dir_zoom_level[iz].absoluteFilePath());
        QFileInfoList dir_1 = dir_tmp_1.entryInfoList(QDir::Dirs | QDir::NoSymLinks | QDir::NoDotAndDotDot);
        for (int i1 = 0; i1 < dir_1.size(); ++i1) {
            // y
            int           y = dir_1[i1].baseName().toInt();
            QDir          dir_tmp_2(dir_1[i1].absoluteFilePath());
            QFileInfoList im = dir_tmp_2.entryInfoList(QDir::Files | QDir::NoSymLinks | QDir::NoDotAndDotDot);
            for (int i2 = 0; i2 < im.size(); ++i2) {
                // x
                int       x = im[i2].baseName().toInt();
                imagetile til(x, y, z);
                til.loadfromfile(im[i2].filePath().toLocal8Bit().data());
                _tiles.push_back(std::move(til));

                if (_tiles.size() >= 20000) {
                    insert_imagetile_tombtiles(db, &_tiles);
                }
            }
        }
    }

    insert_imagetile_tombtiles(db, &_tiles);

    sqlite3_close(db);
    qDebug() << "well done";
}

void isource_tile_impl::insert_imagetile_tombtiles(sqlite3* db, imagetile::Contianer* tiles)
{
    qDebug() << tiles->size();
    if (tiles->size() < 1) return;

    sqlite3_stmt* stmt;
    int           status = sqlite3_exec(db, "begin;", 0, 0, 0);
    qDebug() << "begin:" << status;

    char buf[512] = "insert or ignore into tiles values(?,?,?,?)";
    sqlite3_prepare(db, buf, strlen(buf), &stmt, 0);

    imagetile* ptil = 0;
    for (size_t i = 0; i < tiles->size(); ++i) {
        ptil = &(tiles->at(i));

        sqlite3_reset(stmt);
        sqlite3_bind_int(stmt, 1, ptil->zoom_level);
        sqlite3_bind_int(stmt, 2, ptil->tile_column);
        sqlite3_bind_int(stmt, 3, ptil->tile_row);
        sqlite3_bind_blob(stmt, 4, ptil->sp_blob.get(), ptil->blob_size, 0);
        sqlite3_step(stmt);
    }

    status = sqlite3_exec(db, "commit;", 0, 0, 0);

    sqlite3_finalize(stmt);
    qDebug() << "commit:" << status;
    tiles->resize(0);
}
