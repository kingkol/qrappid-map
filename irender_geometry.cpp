﻿#include "irender_geometry.h"

#include "rappidprojection.h"
iRender_geometry::iRender_geometry() : geom_type(geom_point), geom_id(0), geom_name(""), ico_draw(false), prop_rotate(NAN) {}

iRender_geometry* iRender_geometry::copydata(lineString& coors)
{
    if (coors.size() < 1) return this;
    resize(coors.size());
    memcpy(data(), coors.data(), coors.size() * sizeof(Coordinate));
    return this;
}
void iRender_geometry::toscreen(int zoomlevel, QPointF* offset)
{
    if (draw_data.size() != this->size()) {
        draw_data.resize(this->size());
    }
    int ncnt = int(this->size());
    for (int i = 0; i < ncnt; ++i) {
        draw_data.at(i).rx() = RappidProjection::lngToPixel(this->at(i).rx(), zoomlevel) + offset->rx();
        draw_data.at(i).ry() = RappidProjection::latToPixel(this->at(i).ry(), zoomlevel) + offset->ry();
    }
}
iRender_polygon::iRender_polygon()
{
    geom_type = geom_polygon;
    sprintf(geom_name, "polygon");
}

iRender_linestring::iRender_linestring()
{
    geom_type = geom_line;
    sprintf(geom_name, "linestring");
}

iRender_points::iRender_points()
{
    geom_type = geom_point;
    sprintf(geom_name, "points");
}

iRender_points* iRenderCaster::ToPoints(iRender_geometry::iHRender_geometry obj)
{
    return ((obj && (obj->geom_type == geom_point)) ? static_cast<iRender_points*>(obj.get()) : nullptr);
}

iRender_polygon* iRenderCaster::ToPolygon(iRender_geometry::iHRender_geometry obj)
{
    return ((obj && (obj->geom_type == geom_polygon)) ? static_cast<iRender_polygon*>(obj.get()) : nullptr);
}

iRender_linestring* iRenderCaster::ToLinestring(iRender_geometry::iHRender_geometry obj)
{
    return ((obj && (obj->geom_type == geom_line)) ? static_cast<iRender_linestring*>(obj.get()) : nullptr);
}
