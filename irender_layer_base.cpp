﻿#include "irender_layer_base.h"

#include <QDebug>
#include <cmath>

#include "rappidprojection.h"

iRender_layer_base::iRender_layer_base() : _painter_handle(0), CustomTheme(0)
{
    Thame.Line       = QPen(Qt::green, 3, Qt::PenStyle::SolidLine);
    Thame.Outline    = Qt::NoPen;  // QPen(Qt::red, 1, Qt::PenStyle::DashDotDotLine);
    Thame.Font       = QPen(Qt::black, 2);
    Thame.Fill       = QBrush(QColor(157, 204, 255, 100));
    Thame.Ico        = QImage(":/res/yaw0.png").scaled(40, 40);
    Thame.Ico_anchor = QPointF(Thame.Ico.width() * 0.5, 30.);
}

void iRender_layer_base::paint()
{
    if (!_painter_handle) return;

    if (_data_feature.empty()) return;

    //
    iRender_geometry::iter _iter     = _data_feature.begin();
    iRender_geometry*      _geometry = 0;
    for (; _iter != _data_feature.end(); ++_iter) {
        _geometry = _iter->get();
        // 至少有一个点
        if (_geometry->size() < 1) continue;

        //经纬度转画布坐标（y轴朝上，屏幕中心为原点）
        _geometry->toscreen(*_painter_handle->ptr_zoomlevel, _painter_handle->ptr_offset);

        if (CustomTheme) {
            CustomTheme(&Thame, _geometry->geom_id);
        };

        if (geom_polygon == _geometry->geom_type) {
            // 3点构面
            if (_geometry->size() < 3) continue;

            _painter_handle->ptr_painter->setPen(Thame.Outline);
            _painter_handle->ptr_painter->setBrush(Thame.Fill);

#if 1
            //简单面
            _painter_handle->ptr_painter->drawPolygon(_geometry->drawdata(), _geometry->size());
#else
            //三角带，n多个三角形
            for (size_t i = 0; i < data_to_draw.size() - 2; ++i) {
                _painter_handle->ptr_painter->drawPolygon(data_to_draw.data() + i, 3);
            }
#endif
        } else if (geom_line == _geometry->geom_type) {
            // 2点构线
            if (_geometry->size() < 2) continue;

            _painter_handle->ptr_painter->setPen(Thame.Line);
            _painter_handle->ptr_painter->setBrush(Qt::red);

            _painter_handle->ptr_painter->drawPolyline(_geometry->drawdata(), _geometry->size());
        } else if (geom_point == _geometry->geom_type) {
            QPointF* points = _geometry->drawdata();
            //
            if (_geometry->ico_draw) {
                _painter_handle->ptr_painter->setPen(Qt::NoPen);
                _painter_handle->ptr_painter->setBrush(Qt::NoBrush);

                _painter_handle->ptr_painter->save();

                if (!std::isnan(_geometry->prop_rotate)) {
                    //顺时针 0 - 360
                    _painter_handle->ptr_painter->rotate(_geometry->prop_rotate);
                }

                QPointF xyico;
                for (size_t i = 0; i < _geometry->size(); ++i) {
                    xyico = points[i];

                    //旋转后的新坐标
                    if (!std::isnan(_geometry->prop_rotate)) {
                        qreal _rad = _geometry->prop_rotate * D_TO_RAD;
                        xyico.setX(points[i].rx() * cos(_rad) + points[i].ry() * sin(_rad));
                        xyico.setY(points[i].ry() * cos(_rad) - points[i].rx() * sin(_rad));
                    }

                    //减去图像锚点
                    _painter_handle->ptr_painter->drawImage(xyico - Thame.Ico_anchor, Thame.Ico);
                }

                _painter_handle->ptr_painter->restore();

            } else {
                _painter_handle->ptr_painter->setPen(Qt::black /*Thame.Outline*/ /* Qt::NoPen*/);
                //_painter_handle->ptr_painter->setBrush(Qt::red);
#if 0
                _painter_handle->ptr_painter->drawPoints(data_to_draw.data(), data_to_draw.size());
                _painter_handle->ptr_painter->setWorldMatrixEnabled(false);
                for (size_t i = 0; i < data_to_draw.size(); ++i) {
                    QRectF textrect(_painter_handle->ptr_paint_event->rect().width() * 0.5 + data_to_draw[i].x() - txt_rect_width_half,
                                    _painter_handle->ptr_paint_event->rect().height() * 0.5 - data_to_draw[i].y() - txt_rect_height_half, txt_rect_width,
                                    txt_rect_height);

                    _painter_handle->ptr_painter->drawText(textrect, Qt::AlignHCenter | Qt::AlignTop, QString::number(i));
                }
                _painter_handle->ptr_painter->setWorldMatrixEnabled(true);
#else
                for (size_t i = 0; i < _geometry->size(); ++i) {
                    _painter_handle->ptr_painter->drawEllipse(points[i], 5, 5);
                }
#endif
            }
        }
    }
}

void iRender_layer_base::cleargeometry() { _data_feature.clear(); }

iRender_geometry::iHRender_geometry iRender_layer_base::get(int id)
{
    if (_data_feature.empty()) return 0;
    int ncnt = int(_data_feature.size());
    for (int i = 0; i < ncnt; ++i) {
        if (_data_feature.at(i)->geom_id == id) {
            return _data_feature.at(i);
        }
    }
    return 0;
}

iRender_geometry::iHRender_geometry iRender_layer_base::get(const char* nm)
{
    if (_data_feature.empty() || nm == nullptr || strlen(nm) < 1) return 0;
    for (size_t i = 0; i < _data_feature.size(); ++i) {
        if (memcmp(_data_feature.at(i)->geom_name, nm, strlen(nm)) == 0) {
            return _data_feature.at(i);
        }
    }
    return 0;
}

iRender_geometry::iHRender_geometry iRender_layer_base::addgeometry(int _type)
{
    iRender_geometry::iHRender_geometry hgeom = 0;
    if (geom_line == _type) {
        hgeom = std::make_shared<iRender_linestring>();
    } else if (geom_polygon == _type) {
        hgeom = std::make_shared<iRender_polygon>();
    } else if (geom_point == _type) {
        hgeom = std::make_shared<iRender_points>();
    }
    if (hgeom == 0) return 0;

    _data_feature.push_back(hgeom);
    return hgeom;
}

iRender_layer::iRender_layer() {}
