﻿#ifndef IRENDER_GEOMETRY_H
#define IRENDER_GEOMETRY_H

#include <QPointF>
#include <memory>
#include <vector>

#define LAYER_NAME_SIZE 32

enum
{
    geom_point,
    geom_line,
    geom_polygon,
};

// lng,lat
typedef QPointF              Coordinate;
typedef std::vector<QPointF> lineString;

class iRender_geometry : public lineString
{
public:
    typedef std::shared_ptr<iRender_geometry> iHRender_geometry;
    typedef std::vector<iHRender_geometry>    Container;
    typedef Container::iterator               iter;

public:
    iRender_geometry();

    inline void set_type(int _type) { geom_type = _type; }
    inline void set_id(int _id) { geom_id = _id; }
    inline void set_name(const char* _name) { sprintf(geom_name, "%s", _name); }

    inline long add_coordinate(Coordinate* coord)
    {
        emplace_back(coord->x(), coord->y());
        return (long)size();
    }
    inline long add_coordinate(double x, double y)
    {
        emplace_back(x, y);
        return (long)size();
    }
    //    inline long add_coordinate(double& x, double& y)
    //    {
    //        emplace_back(x, y);
    //        return (long)size();
    //    }

    iRender_geometry* copydata(lineString& coors);

    void toscreen(int zoomlevel, QPointF* offset);

    inline QPointF* drawdata() { return draw_data.data(); }

private:
    lineString draw_data;

public:
    int  geom_type;
    int  geom_id;
    char geom_name[LAYER_NAME_SIZE];

public:
    bool  ico_draw;
    float prop_rotate;
};

class iRender_polygon : public iRender_geometry
{
public:
    iRender_polygon();
};

class iRender_linestring : public iRender_geometry
{
public:
    iRender_linestring();
};

class iRender_points : public iRender_geometry
{
public:
    iRender_points();
};

class iRenderCaster
{
public:
    static iRender_points*     ToPoints(iRender_geometry::iHRender_geometry obj);
    static iRender_polygon*    ToPolygon(iRender_geometry::iHRender_geometry obj);
    static iRender_linestring* ToLinestring(iRender_geometry::iHRender_geometry obj);
};

#endif  // IRENDER_GEOMETRY_H
