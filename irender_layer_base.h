﻿#ifndef IRENDER_LAYER_BASE_H
#define IRENDER_LAYER_BASE_H

#include <QBrush>
#include <QFont>
#include <QPaintEvent>
#include <QPainter>
#include <QPainterPath>
#include <QPen>
#include <QWidget>
#include <functional>

#include "irender_geometry.h"

class iRender_painter
{
public:
    iRender_painter()
    {
        ptr_painter     = 0;
        ptr_paint_event = 0;
        ptr_offset      = 0;
        ptr_zoomlevel   = 0;
    }

public:
    QPainter    *ptr_painter;
    QPaintEvent *ptr_paint_event;
    QPointF     *ptr_offset;
    int         *ptr_zoomlevel;
};

class iRender_style
{
public:
    typedef std::function<void(iRender_style *, int id)> RenderThame;
    typedef QPen                                         HOutline;
    typedef QPen                                         HLine;
    typedef QPen                                         HFont;
    typedef QBrush                                       HFill;
    typedef QImage                                       HIco;

public:
    HOutline Outline;
    HLine    Line;
    HFill    Fill;
    HFont    Font;
    HIco     Ico;
    QPointF  Ico_anchor;
};

class iRender_layer_base
{
public:
    iRender_layer_base();

public:
    virtual void paint();

    virtual iRender_geometry::iHRender_geometry addgeometry(int _type);

public:
    virtual void cleargeometry();

    virtual iRender_geometry::iHRender_geometry get(int id);

    virtual iRender_geometry::iHRender_geometry get(const char *nm);

public:
    iRender_painter            *_painter_handle;
    iRender_geometry::Container _data_feature;

public:
    iRender_style::RenderThame CustomTheme;
    iRender_style              Thame;
};

class iRender_layer
{
public:
    typedef std::shared_ptr<iRender_layer_base> iHRender_layer;
    typedef std::vector<iHRender_layer>         Container;

public:
    iRender_layer();
};

#endif  // IRENDER_LAYER_BASE_H
