﻿#include "mainwindow.h"

#include <QBoxLayout>
#include <QDebug>
#include <QTextStream>
#include <thread>

#include "irender_layer_vector.h"
#include "irender_widget_imagetile.h"
#include "ui_mainwindow.h"
MainWindow::MainWindow(QWidget* parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QHBoxLayout*              mainlayout   = new QHBoxLayout();
    iRender_widget_imagetile* ptile_widget = new iRender_widget_imagetile();

    bglayer = std::make_shared<iRender_layer_vector>();

    ptile_widget->addlayer(bglayer);
    mainlayout->addWidget(ptile_widget);
    ui->centralwidget->setLayout(mainlayout);

    iRender_layer_vector* veclay = (iRender_layer_vector*)bglayer.get();

    iRender_geometry::iHRender_geometry ovlcent = veclay->addgeometry(geom_point);
    //基站中心点
    // ovlcent->add_coordinate(114.300189, 27.585654);
    veclay->add_eclipse(Coordinate(114.300189, 30.585654), 30000);

    std::thread* t = new std::thread([=]() {
        iRender_geometry::iHRender_geometry g = 0;
        g                                     = bglayer->addgeometry(geom_line);
        pyaw                                  = bglayer->addgeometry(geom_point);
        pyaw->ico_draw                        = true;
        int         i                         = 0;
        QFile       t("D:/fullmark.csv");
        QTextStream d(&t);
        bool        ret = t.open(QIODevice::ReadOnly);
        while (ret) {
            QString _line = d.readLine();
            if (_line.length() < 1) break;

            double     _dtmp, _lat, _lng, _heading;
            QByteArray _linearray = _line.toLocal8Bit();
            sscanf(_linearray.data(), "%lf,%lf,%lf,%lf,%lf,%lf", &_dtmp, &_lat, &_lng, &_dtmp, &_dtmp, &_heading);

            // iMap_Engine::iMap_tools::wgs84togcj(tmp.lat, tmp.lng, tmp.lat, tmp.lng);
            if (i++ % 1 == 0) {
                g->add_coordinate(_lng, _lat);

                if (pyaw->size() < 1) {
                    pyaw->add_coordinate(_lng, _lat);
                } else {
                    pyaw->prop_rotate = /*180 - */ _heading;
                    pyaw->at(0)       = Coordinate(_lng, _lat);
                }
                // ptile_widget->centerOn(_lng, _lat);
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
            ptile_widget->signal_update();
        }
    });
    t->detach();
}

MainWindow::~MainWindow() { delete ui; }
