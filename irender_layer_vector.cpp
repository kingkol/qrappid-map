﻿#include "irender_layer_vector.h"

#include "rappidprojection.h"

iRender_layer_vector::iRender_layer_vector() {}

void iRender_layer_vector::add_eclipse(Coordinate cent, float radius)
{
    lineString vetexs;

    double x, y, b, l;
    RappidProjection::lonLat2Mercator(cent.rx(), cent.ry(), x, y);

    int   nslice  = 64;
    float incrase = 2 * M_PI / float(nslice);
    for (int i = 0; i < nslice; ++i) {
        float rot = incrase * i;

        Coordinate mc(radius * sin(rot) + x, radius * cos(rot) + y);
        RappidProjection::Mercator2lonLat(mc.rx(), mc.ry(), l, b);

        vetexs.emplace_back(l, b);
    }

    addgeometry(geom_polygon)->copydata(vetexs);
}
