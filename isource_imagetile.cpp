﻿#include "isource_imagetile.h"

#include <QDebug>
#include <QFile>
#include <QString>
#include <chrono>

#include <curl/curl.h>

QPointF tilekey::tp;

isource_tile_sqlite::isource_tile_sqlite()
{
    // std::string filename = QStringLiteral(R"(D:\china13.mbtiles)").toStdString();
    std::string filename = QStringLiteral(R"(D:\TData\TileMapData\WXWP\world.mbtiles)").toStdString();
    int         status   = sqlite3_open(filename.c_str(), &this->db);
    qDebug() << "sqlite3_open" << status;
    if (status) {
        sqlite3_close(this->db);
    }
}

isource_tile_sqlite::~isource_tile_sqlite()
{
    if (db) sqlite3_close(this->db);
}

QImage* isource_tile_sqlite::get_tile(tilekey _key)
{
    if (_cache.size() > 200) _cache.clear();
    QString str = tilekey::keytostr(_key);
    if (!_cache.contains(str)) {
        QImage _im;
#if 0
        QString pngfile = QString::asprintf(R"(D:\TData\TileMapData\AA\mapabc\roadmap\%d\%d\%d.png)",
                                            _key.tilez, _key.tilex, _key.tiley);
        if (QFile::exists(pngfile)) {
            std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();
            if (_im.load(pngfile, "png")) {
                std::chrono::duration<double, std::milli> delat1 = std::chrono::steady_clock::now() - t1;
                qDebug() << QString::asprintf("load: %.3f", delat1.count() * 0.001);
                _cache.insert(str, _im);
                // qDebug() << _cache.size();
            }
        }
#else
        sqlite3_stmt *stmt   = NULL;
        int           status = sqlite3_prepare(this->db,
                                               "SELECT tile_data FROM tiles WHERE zoom_level = ? AND tile_column = ? AND tile_row = ?;",  // stmt
                                               -1,  // If than zero, then stmt is read up to the first nul terminator
                                               &stmt,
                                               0  // Pointer to unused portion of stmt
                                               );
        if (status != SQLITE_OK) {
            printf("Could not prepare statement\n");
        }

        status = sqlite3_bind_int(stmt, 1, _key.tilez);
        if (status != SQLITE_OK) {
            printf("Could not bind value to statement\n");
        }

        status = sqlite3_bind_int(stmt, 2, _key.tilex);
        if (status != SQLITE_OK) {
            printf("Could not bind value to statement\n");
        }

        status = sqlite3_bind_int(stmt, 3, _key.tiley);
        if (status != SQLITE_OK) {
            printf("Could not bind value to statement\n");
        }

        // qDebug() << _key.tilez << _key.tilex << _key.tiley;

        // std::chrono::steady_clock::time_point t1       = std::chrono::steady_clock::now();
        status        = SQLITE_ROW;
        bool firstRow = true;
        while (status == SQLITE_ROW) {
            status = sqlite3_step(stmt);
            if (firstRow && status == SQLITE_DONE) {
                sqlite3_finalize(stmt);
                stmt = 0;
                break;
            }

            if (status == SQLITE_ROW) {
                int         numBytes = sqlite3_column_bytes(stmt, 0);
                const void *blobRaw  = sqlite3_column_blob(stmt, 0);

                bool ret = _im.loadFromData((uchar *)blobRaw, numBytes);
                qDebug() << numBytes << ret;
                _cache.insert(str, _im);
                sqlite3_finalize(stmt);

                stmt = 0;
                break;
            }
            firstRow = false;
        }
        //        std::chrono::duration<double, std::milli> delat1 = std::chrono::steady_clock::now() - t1;
        //        qDebug() << QString::asprintf("load: %.3f    %d", delat1.count() * 0.001, ncounter);
        if (stmt) sqlite3_finalize(stmt);
#endif
    }
    if (!_cache.contains(str)) return 0;
    return &(_cache[str]);
}

isource_tile_http_arcgis::isource_tile_http_arcgis(){}

isource_tile_http_arcgis::~isource_tile_http_arcgis(){}

QImage* isource_tile_http_arcgis::get_tile(tilekey _key) {
    if (_cache.size() > 200) _cache.clear();
    QString str = tilekey::keytostr(_key);
    if (!_cache.contains(str)) {

#if 0
        QString pngfile = QString::asprintf(R"(D:\TData\TileMapData\AA\mapabc\roadmap\%d\%d\%d.png)",
                                            _key.tilez, _key.tilex, _key.tiley);
        if (QFile::exists(pngfile)) {
            std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();
            if (_im.load(pngfile, "png")) {
                std::chrono::duration<double, std::milli> delat1 = std::chrono::steady_clock::now() - t1;
                qDebug() << QString::asprintf("load: %.3f", delat1.count() * 0.001);
                _cache.insert(str, _im);
                // qDebug() << _cache.size();
            }
        }
#else
        //"https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/19/195303/419251"
        std::string sUrl = "https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/";
        std::string sz = std::to_string(_key.tilez);
        std::string sx = std::to_string(_key.tilex);
        std::string sy = std::to_string(_key.tiley);

        sUrl = sUrl + sz + "/" + sy + "/" + sx;

        //sUrl = "https://services.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/18/102800/232858";

        CURL *pCurl;
        CURLcode ret;

        pCurl = curl_easy_init();
        curl_easy_setopt(pCurl, CURLOPT_WRITEDATA, (void*)this);
        curl_easy_setopt(pCurl, CURLOPT_WRITEFUNCTION, curl_writefunc);

        curl_easy_setopt(pCurl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_easy_setopt(pCurl, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_easy_setopt(pCurl, CURLOPT_URL, sUrl.c_str());
        ret = curl_easy_perform(pCurl);
        curl_easy_cleanup(pCurl);
#endif
    }
    if (!_cache.contains(str)) return nullptr;
    return &(_cache[str]);
}

void isource_tile_http_arcgis::get_tiles(tileDB* _key){
    tileDB::iterator _itr = _key->begin();
    while (_itr != _key->end()){
        //

        //"https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/19/195303/419251"
        std::string sUrl = "https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/";
        std::string sz = std::to_string(_itr->tilez);
        std::string sx = std::to_string(_itr->tilex);
        std::string sy = std::to_string(_itr->tiley);

        sUrl = sUrl + sz + "/" + sy + "/" + sx;

        //sUrl = "https://services.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/18/102800/232858";

        std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();

        CURL *pCurl;
        CURLcode ret;

        pCurl = curl_easy_init();
        curl_easy_setopt(pCurl, CURLOPT_WRITEDATA, (void*)&(*_itr));
        curl_easy_setopt(pCurl, CURLOPT_WRITEFUNCTION, curl_writefunc);

        curl_easy_setopt(pCurl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_easy_setopt(pCurl, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_easy_setopt(pCurl, CURLOPT_URL, sUrl.c_str());
        ret = curl_easy_perform(pCurl);

        std::chrono::duration<double, std::milli> delat1 = std::chrono::steady_clock::now() - t1;
        fprintf(stderr, "load: %3d ms", (int)delat1.count());

        curl_easy_cleanup(pCurl);

        fprintf(stderr, "%d %s\n",ret, sUrl.c_str());
        _itr++;
    }
}

size_t isource_tile_http_arcgis::curl_writefunc(char *buffer, size_t size, size_t nitems, void *user)
{
    tilekey* p = (tilekey*)user;
    p->loadimage(buffer, nitems);
    return nitems;
}
