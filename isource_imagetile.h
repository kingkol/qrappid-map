#ifndef ISOURCE_IMAGETILE_H
#define ISOURCE_IMAGETILE_H

extern "C"
{
#include <sqlite3.h>
}

#include <QPainter>
#include <QImage>
#include <QMap>
#include <QString>

class tilekey
{
public:
    tilekey()
    {
        tilex = 0;
        tiley = 0;
        tilez = 0;
        img = nullptr;
    }
    tilekey(int x, int y, int z)
    {
        tilex = x;
        tiley = y;
        tilez = z;
        img = nullptr;
    }

    bool operator<(const tilekey& item) const
    {
        if (tilez != item.tilez) return true;
        if (tiley != item.tiley) return true;
        if (tilex != item.tilex) return true;
        return false;
    }

    bool loadimage(char *blob, size_t size){
        if (img == nullptr)
            img = QSharedPointer<QImage>(new QImage);
        bool ret = img->loadFromData((uchar *)blob, size);
        return ret;
    }

    void draw(QPainter *painter){
        if (img == nullptr)
            return;
        QPoint pstart(tilex * 256, tiley * 256);
        pstart += tp.toPoint();
        painter->drawImage(pstart, *img);
    }

    static QString keytostr(tilekey _key) { return QString::asprintf("%d#%d#%d", _key.tilez, _key.tiley, _key.tilex); }
    static QString keytostr(int x, int y, int z) { return keytostr(tilekey(x, y, z)); }

public:
    static QPointF tp;

    bool visible;
    int tilex;
    int tiley;
    int tilez;    
    QSharedPointer<QImage> img;
};

typedef QMap<QString, QImage> geotile;

typedef QMap<QString, tilekey> tileDB;

class isource_tile
{
public:
    isource_tile(){}
    virtual ~isource_tile(){}
    virtual QImage* get_tile(tilekey _key) = 0;
    virtual void get_tiles(tileDB* _key) = 0;

public:
    geotile  _cache;
};

class isource_tile_sqlite :public isource_tile
{
public:
    isource_tile_sqlite();
    ~isource_tile_sqlite();
    QImage* get_tile(tilekey _key) override;
    void get_tiles(tileDB* _key){}

private:
    sqlite3* db;
};

class isource_tile_http_arcgis :public isource_tile
{
public:
    isource_tile_http_arcgis();
    ~isource_tile_http_arcgis();
    QImage* get_tile(tilekey _key) override;
    void get_tiles(tileDB* _key) override;
public:
    static size_t curl_writefunc(char *buffer, size_t size, size_t nitems, void *user);
};

#endif  // ISOURCE_IMAGETILE_H
