﻿#ifndef RAPPIDPROJECTION_H
#define RAPPIDPROJECTION_H

#ifndef M_PI
#define M_PI 3.141592653589793
#endif

const double M_PI_Q_2       = (M_PI / 2.);
const double M_PI_Q_360     = (M_PI / 360.);
const double M_PI_Q_180     = (M_PI / 180.);
const double Q_180_M_PI     = (180. / M_PI);
const double D_TO_RAD       = M_PI_Q_180;
const double RAD_TO_D       = Q_180_M_PI;
const double EARTH_N        = (20037508.34);
const double EARTH_N_Q_180  = (EARTH_N / 180.);
const double Q_180_EARTH_N  = (180. / EARTH_N);
const double EQUATOR_LENGTH = 40075.016686;  // km

class RappidProjection
{
public:
    RappidProjection();

public:
    static int long2tilex(double lon, int z);
    static int lat2tiley(double lat, int z);

    static double tilex2long(int x, int z);
    static double tiley2lat(int y, int z);

    static void lonLat2Mercator(double lng, double lat, double &mcx, double &mcy);
    static void Mercator2lonLat(double mcx, double mcy, double &lng, double &lat);

    static double getresolution(double lat, int z);
    static double getscale(int screendpi, double reso);

    static double lngToPixel(double lng, int zoom);
    static double pixelToLng(double pixelX, int zoom);
    static double latToPixel(double lat, int zoom);
    static double pixelToLat(double pixelY, int zoom);
};

#endif  // RAPPIDPROJECTION_H
