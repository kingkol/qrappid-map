QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11 console

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    irender_geometry.cpp \
    irender_layer_base.cpp \
    irender_layer_label.cpp \
    irender_layer_vector.cpp \
    irender_widget_imagetile.cpp \
    isource_imagetile.cpp \
    isource_tile_impl.cpp \
    main.cpp \
    mainwindow.cpp \
    rappidprojection.cpp

HEADERS += \
    irender_geometry.h \
    irender_layer_base.h \
    irender_layer_label.h \
    irender_layer_vector.h \
    irender_widget_imagetile.h \
    isource_imagetile.h \
    isource_tile_impl.h \
    mainwindow.h \
    rappidprojection.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DEPENDPATH += $$PWD/third-party/vc142

#sqlite
INCLUDEPATH +=$$DEPENDPATH/sqlite3/include
LIBS += -L$$DEPENDPATH/sqlite3/lib -lsqlite3
#curl
INCLUDEPATH +=$$DEPENDPATH/CURL/include
LIBS += -L$$DEPENDPATH/CURL/lib -llibcurl_imp

RESOURCES += \
    qrappidmap.qrc
