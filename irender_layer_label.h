#ifndef IRENDER_LAYER_LABEL_H
#define IRENDER_LAYER_LABEL_H

#include "irender_layer_base.h"

class iRender_layer_label : public iRender_layer_base
{
public:
    iRender_layer_label();

    void paint() override;
};

#endif  // IRENDER_LAYER_LABEL_H
