#include "irender_layer_label.h"

iRender_layer_label::iRender_layer_label() {}

void iRender_layer_label::paint()
{
    if (!_painter_handle) return;

    if (_data_feature.empty()) return;

    //
    _painter_handle->ptr_painter->setWorldMatrixEnabled(false);
    _painter_handle->ptr_painter->setPen(Thame.Font);
    iRender_geometry::iter _iter = _data_feature.begin();

    iRender_geometry* _geometry = 0;
    for (; _iter != _data_feature.end(); ++_iter) {
        _geometry = _iter->get();
        // 至少有一个点 并且有名字
        if (_geometry->size() < 1 || strlen(_geometry->geom_name) < 1) continue;

        _geometry->toscreen(*_painter_handle->ptr_zoomlevel, _painter_handle->ptr_offset);

        QPointF* points = _geometry->drawdata();
        int      locx = points[0].rx(), locy = points[0].ry();
        if (locx < 0 || locx > _painter_handle->ptr_paint_event->rect().width() || locy < 0 || locy > _painter_handle->ptr_paint_event->rect().height()) {
            continue;
        }
        if (CustomTheme) {
            CustomTheme(&Thame, _geometry->geom_id);
        };

        QRectF textrect(locx - 50, locy - 15, 100, 30);
        //_painter_handle->ptr_painter->drawRect(textrect);
        _painter_handle->ptr_painter->drawText(textrect, Qt::AlignHCenter | Qt::AlignTop, QString::fromLocal8Bit(_geometry->geom_name));
    }
    _painter_handle->ptr_painter->setWorldMatrixEnabled(true);
}
